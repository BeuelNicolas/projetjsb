let express = require('express');
let consolidate = require('consolidate');
let session = require('express-session');
let bodyParser = require("body-parser");
let http = require('http');


let app = express ();

app.engine ( 'html', consolidate.hogan);
app.set('views', 'static');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    secret : "JSB",
    resave : false,
    saveUninitialized : true,
    cookie : {
        path: '/',
        httpOnly: true,
        maxAge: 3600000
    }
}));
// Your code here

app.get('/', (req, res) => {
    //res.status(200).send('Ca marche');
    res.render('html/index.html', {userLogin:"<a href=''>Log in</a>"})
});

app.post('/', (req, res) => {
    console.log(req.body.username);
    console.log(req.body.password);
    let err = ""
    if(req.body.password === "123pass"){
        req.session.username = req.body.username
    }else {
        req.session.username = ""
        err = "Mot de passe invalide"
    }

    res.render('html/index.html', {err:err , username: req.session.username})
});

app.get('/inscri', (req, res) => {
    console.log(req.query.username);
    console.log(req.query.password);
    console.log(req.query.name);
    console.log(req.query.email);

    res.redirect('/')
})

app.get('/jjj', (req, res) => {
    res.status(200).send(req.session.username);
});

//End of your code

app.use(express.static('static/assets'));
app.listen(9999);
